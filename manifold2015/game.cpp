﻿// Template, major revision 5
// IGAD/NHTV - Jacco Bikker - 2006-2014

#include "string.h"
#include "surface.h"
#include "stdlib.h"
#include "template.h"
#include "game.h"
#include "freeimage.h"

using namespace Tmpl8;

#define GUIDES				3			// normals (XYZ), sample positions (XYZ), albedo (RGB)
#define SIGMA_H				12.0f
#define SIGMA_R				0.05f		// for RF filter
#define MANDEPTH			3			// 0: 1 manifold; 1: 3 manifolds; 2: 5 manifolds; 3: 15 manifolds
#define OUTLIERTHRESHOLD	0.001f

#define TIME_INIT	1
#define TIME_RF		1
#define TIME_LM		1
#define TIME_TOT	1

#define RECURSE_MANIFOLDS_OP 1
#define RF_OP				1
#define RF_GPGPU			0
#define RF_SIMD				1

// Timers
Timer init_timer, rf_timer, lm_timer, tot_timer;

struct Sample
{
	float3 E;							// illumination at pos
	float3 pos;							// position of primary intersection point
	uint iN, di, dum;					// normal at pos (10 bit per component integer representation)
	float3 color;						// material color at pos
};

// cluster:
// for each original sample i, cluster[i] is non-zero if the sample belongs to the cluster.

float3* N = new float3[PIXELS * SPP];
float3* pos = new float3[PIXELS * SPP];
float3* albedo = new float3[PIXELS * SPP];
float3* noisy = new float3[PIXELS * SPP];

Sample* samples = 0;
float* clusters[9];
float* clusterAvg = new float[PIXELS];
float* blurredAvg = new float[PIXELS];
float3* boxFiltered = new float3[PIXELS];
float3* manifold = new float3[PIXELS * GUIDES];
float* weight = new float[PIXELS * SPP];
float4* splat = new float4[PIXELS];
float3* dIcdx = new float3[PIXELS * GUIDES];
float3* dIcdy = new float3[PIXELS * GUIDES];
float* dHdx = new float[PIXELS];
float* dVdy = new float[PIXELS];
float3* result = new float3[PIXELS];
float4* F = new float4[PIXELS * SPP];
float* coc = new float[PIXELS * SPP];
float* dist = new float[PIXELS * SPP];
float4** lmanifold;	// linear manifolds
float4** manSAT;	// linear manifold SATs
float** coverage;	// linear manifolds, coverage
float* currentw = new float[PIXELS];
float4* LF = new float4[PIXELS];
int manifolds = 0;

struct float9 { float v[9]; };

#if RF_GPGPU == 1
Kernel* rf_y_func;
Kernel* rf_x_func;

static int rf_y_worksize = SCRHEIGHT;
static int rf_x_worksize = SCRWIDTH;
#endif	   


void h_filter_manifold(float sigma)
{
	float a = expf(-sqrtf(2.0f) / sigma); // ~0.8888
	for (int y = 0; y < SCRHEIGHT; y++)
	{
		for (int x = 1; x < SCRWIDTH; x++)
		{
			float* values = (float*)&manifold[(x + y * SCRWIDTH) * GUIDES];
			for (int i = 0; i < GUIDES * 3; i++) values[i] += a * (values[i - GUIDES * 3] - values[i]);
		}
		for (int x = SCRWIDTH - 2; x >= 0; x--)
		{
			float* values = (float*)&manifold[(x + y * SCRWIDTH) * GUIDES];
			for (int i = 0; i < GUIDES * 3; i++) values[i] += a * (values[i + GUIDES * 3] - values[i]);
		}
	}
	for (int x = 0; x < SCRWIDTH; x++)
	{
		for (int y = 1; y < SCRHEIGHT; y++)
		{
			float* values = (float*)&manifold[(x + y * SCRWIDTH) * GUIDES];
			for (int i = 0; i < GUIDES * 3; i++) values[i] += a * (values[i - SCRWIDTH * GUIDES * 3] - values[i]);
		}
		for (int y = SCRHEIGHT - 2; y >= 0; y--)
		{
			float* values = (float*)&manifold[(x + y * SCRWIDTH) * GUIDES];
			for (int i = 0; i < GUIDES * 3; i++) values[i] += a * (values[i + SCRWIDTH * GUIDES * 3] - values[i]);
		}
	}
}

void h_filter_clusteravg(float sigma)
{
	float a = expf(-sqrtf(2.0f) / sigma);
	memcpy(blurredAvg, clusterAvg, PIXELS * sizeof(float));
	for (int y = 0; y < SCRHEIGHT; y++)
	{
		for (int idx = 1 + y * SCRWIDTH, x = 1; x < SCRWIDTH; x++, idx++)
			blurredAvg[idx] += a * (blurredAvg[idx - 1] - blurredAvg[idx]);
		for (int idx = (SCRWIDTH - 2) + y * SCRWIDTH, x = SCRWIDTH - 2; x >= 0; x--, idx--)
			blurredAvg[idx] += a * (blurredAvg[idx + 1] - blurredAvg[idx]);
	}
	for (int x = 0; x < SCRWIDTH; x++)
	{
		for (int idx = x + SCRWIDTH, y = 1; y < SCRHEIGHT; y++, idx += SCRWIDTH)
			blurredAvg[idx] += a * (blurredAvg[idx - SCRWIDTH] - blurredAvg[idx]);
		for (int idx = x + (SCRHEIGHT - 2) * SCRWIDTH, y = SCRHEIGHT - 2; y >= 0; y--, idx -= SCRWIDTH)
			blurredAvg[idx] += a * (blurredAvg[idx + SCRWIDTH] - blurredAvg[idx]);
	}
}

float __powf(const float a, const float b)
{
	union { float d; int x; } u = { a };
	u.x = (int)(b * (u.x - 1064866805) + 1064866805);
	return u.d;
}

#if RF_OP
void RF_filter_splats()
{
	int idx;
	float sig = SIGMA_H / SIGMA_R;
	for (int g = 0; g < GUIDES; g++)
	{
		for (int y = 0; y < SCRHEIGHT - 1; y++)
		{
			for (int x = 0; x < SCRWIDTH - 1; x++)
			{
				idx = (y * SCRWIDTH + x) * GUIDES + g;
				dIcdx[idx] = (manifold[idx + GUIDES] - manifold[idx]) * sig;

				idx = (x + y * SCRWIDTH) * GUIDES + g;
				dIcdy[idx] = (manifold[idx + SCRWIDTH * GUIDES] - manifold[idx]) * sig;
			}
			dIcdx[(y * SCRWIDTH + (SCRWIDTH - 1)) * GUIDES + g] = float3(0, 0, 0);

			idx = ((SCRWIDTH - 1) + y * SCRWIDTH) * GUIDES + g;
			dIcdy[idx] = (manifold[idx + SCRWIDTH * GUIDES] - manifold[idx]) * sig;
		}

		for (int x = 0; x < SCRWIDTH; x++)
		{
			dIcdy[(x + (SCRHEIGHT - 1) * SCRWIDTH) * GUIDES + g] = float3(0, 0, 0);
		}

	}

	for (int y = 0; y < SCRHEIGHT; y++)
	{
		for (int x = 0; x < SCRWIDTH; x++)
		{
			const float* xvalues = (float*)&dIcdx[(x + y * SCRWIDTH) * GUIDES];
			const float* yvalues = (float*)&dIcdy[(x + y * SCRWIDTH) * GUIDES];
			float dIdx = 0, dIdy = 0;
			for (int i = 0; i < (GUIDES * 3); i++) dIdx += xvalues[i] * xvalues[i], dIdy += yvalues[i] * yvalues[i];
			dHdx[x + y * SCRWIDTH] = sqrtf(1.0f + dIdx);
			dVdy[x + y * SCRWIDTH] = sqrtf(1.0f + dIdy);
		}
	}

	float sigma[2];
	sigma[0] = SIGMA_H * sqrtf(3.0f) * 2.0f / sqrtf(15);
	sigma[1] = SIGMA_H * sqrtf(3.0f) * 1.0f / sqrtf(15);

#if RF_GPGPU == 1
	for (int i = 0; i < 2; i++) // two RF iterations
	{
		float a = expf(-sqrtf(2.0f) / sigma[i]);

		Buffer* splat_buffer = new Buffer(sizeof(float4)*PIXELS, Buffer::DEFAULT, splat);
		Buffer* dHdx_buffer = new Buffer(sizeof(float)*PIXELS, Buffer::DEFAULT, dHdx);
		Buffer* a_buffer = new Buffer(sizeof(float), Buffer::DEFAULT, &a);

		rf_y_func->SetArgument(0, splat_buffer);
		rf_y_func->SetArgument(1, dHdx_buffer);
		rf_y_func->SetArgument(2, a_buffer);

		rf_x_func->SetArgument(0, splat_buffer);
		rf_x_func->SetArgument(1, dHdx_buffer);
		rf_x_func->SetArgument(2, a_buffer);

		splat_buffer->CopyToDevice();
		dHdx_buffer->CopyToDevice();
		a_buffer->CopyToDevice();

		rf_y_func->Run();
		rf_x_func->Run();

		splat_buffer->CopyFromDevice(true);
	}
#elif RF_SIMD == 1
	for (int i = 0; i < 2; i++) // two RF iterations
	{
		float a = expf(-sqrtf(2.0f) / sigma[i]);

		union { int idx[4]; __m128i idx4; };
		union { float powA[4]; __m128 powA4; };
		union { float adHdx[4]; __m128 adHdx4; };
		union { float result[4]; __m128 result4; };

		__m128i increment = _mm_set_epi32(1, 1, 1, 1);

		//for (int y = 0; y < SCRHEIGHT; y++) this needs something smart if the scrheight isnt divisible of 4
		for (int y = 0; y < SCRHEIGHT; y += 4)
		{
			__m128i idx4 = _mm_set_epi32(y * SCRWIDTH + 1, (y + 1)* SCRWIDTH + 1, (y + 2) * SCRWIDTH + 1, (y + 3) * SCRWIDTH + 1);
			for (int x = 1; x < SCRWIDTH; x++)
			{
				// comment
				powA4 = _mm_set_ps(a, a, a, a);
				adHdx4 = _mm_set_ps(dHdx[idx[0]], dHdx[idx[1]], dHdx[idx[2]], dHdx[idx[3]]);

				result4 = _mm_pow_ps(powA4, adHdx4);

				splat[idx[0]].quad = _mm_add_ps(splat[idx[0]].quad, _mm_mul_ps(result4, _mm_sub_ps(splat[idx[0]].quad, splat[idx[0] - 1].quad)));
				splat[idx[1]].quad = _mm_add_ps(splat[idx[1]].quad, _mm_mul_ps(result4, _mm_sub_ps(splat[idx[1]].quad, splat[idx[1] - 1].quad)));
				splat[idx[2]].quad = _mm_add_ps(splat[idx[2]].quad, _mm_mul_ps(result4, _mm_sub_ps(splat[idx[2]].quad, splat[idx[2] - 1].quad)));
				splat[idx[3]].quad = _mm_add_ps(splat[idx[3]].quad, _mm_mul_ps(result4, _mm_sub_ps(splat[idx[3]].quad, splat[idx[3] - 1].quad)));
				/*
				splat[idx[0]] += powf(a, dHdx[idx[0]]) * (splat[idx[0] - 1] - splat[idx[0]]);
				splat[idx[1]] += powf(a, dHdx[idx[1]]) * (splat[idx[1] - 1] - splat[idx[1]]);
				splat[idx[2]] += powf(a, dHdx[idx[2]]) * (splat[idx[2] - 1] - splat[idx[2]]);
				splat[idx[3]] += powf(a, dHdx[idx[3]]) * (splat[idx[3] - 1] - splat[idx[3]]);
				*/
				idx4 = _mm_add_epi32(idx4, increment);
			}
			for (int idx = y * SCRWIDTH + SCRWIDTH - 2, x = SCRWIDTH - 2; x >= 0; x--, idx--)
			{
				splat[idx] += powf(a, dHdx[idx + 1]) * (splat[idx + 1] - splat[idx]);
			}
		}
		for (int x = 0; x < SCRWIDTH; x++)
		{
			for (int idx = x + SCRWIDTH, y = 1; y < SCRHEIGHT; y++, idx += SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx]) * (splat[idx - SCRWIDTH] - splat[idx]);
			}
			for (int idx = x + (SCRHEIGHT - 2) * SCRWIDTH, y = SCRHEIGHT - 2; y >= 0; y--, idx -= SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx + SCRWIDTH]) * (splat[idx + SCRWIDTH] - splat[idx]);
			}
		}
	}
#else
	for (int i = 0; i < 2; i++) // two RF iterations
	{
		float a = expf(-sqrtf(2.0f) / sigma[i]);
		//printf("%f\n%f\n", __powf(4.0, 0.5), powf(4.0, 0.5));

		for (int y = 0; y < SCRHEIGHT; y++)
		{
			for (int idx = y * SCRWIDTH + 1, x = 1; x < SCRWIDTH; x++, idx++)
			{
				splat[idx] += powf(a, dHdx[idx]) * (splat[idx - 1] - splat[idx]);
			}
			for (int idx = y * SCRWIDTH + SCRWIDTH - 2, x = SCRWIDTH - 2; x >= 0; x--, idx--)
			{
				splat[idx] += powf(a, dHdx[idx + 1]) * (splat[idx + 1] - splat[idx]);
			}
		}
		for (int x = 0; x < SCRWIDTH; x++)
		{
			for (int idx = x + SCRWIDTH, y = 1; y < SCRHEIGHT; y++, idx += SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx]) * (splat[idx - SCRWIDTH] - splat[idx]);
			}
			for (int idx = x + (SCRHEIGHT - 2) * SCRWIDTH, y = SCRHEIGHT - 2; y >= 0; y--, idx -= SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx + SCRWIDTH]) * (splat[idx + SCRWIDTH] - splat[idx]);
			}
		}
	}
#endif // RF_GPGPU


}
#else
void RF_filter_splats()
{
	int idx;
	float sig = SIGMA_H / SIGMA_R;
	for (int y = 0; y < SCRHEIGHT; y++)
	{
		for (int x = 0; x < SCRWIDTH - 1; x++)
		{
			for (int g = 0; g < GUIDES; g++)
			{
				int idx = (y * SCRWIDTH + x) * GUIDES + g;
				dIcdx[idx] = (manifold[idx + GUIDES] - manifold[idx]) * sig;
			}
		}
		for (int g = 0; g < GUIDES; g++)
		{
			dIcdx[(y * SCRWIDTH + (SCRWIDTH - 1)) * GUIDES + g] = float3(0, 0, 0);
		}
	}
	for (int x = 0; x < SCRWIDTH; x++)
	{
		for (int y = 0; y < SCRHEIGHT - 1; y++)
		{
			for (int g = 0; g < GUIDES; g++)
			{
				idx = (x + y * SCRWIDTH) * GUIDES + g;
				dIcdy[idx] = (manifold[idx + SCRWIDTH * GUIDES] - manifold[idx]) * sig;
			}
	}
		for (int g = 0; g < GUIDES; g++)
		{
			dIcdy[(x + (SCRHEIGHT - 1) * SCRWIDTH) * GUIDES + g] = float3(0, 0, 0);
		}
}
	for (int y = 0; y < SCRHEIGHT; y++)
	{
		for (int x = 0; x < SCRWIDTH; x++)
		{
			const float* xvalues = (float*)&dIcdx[(x + y * SCRWIDTH) * GUIDES];
			const float* yvalues = (float*)&dIcdy[(x + y * SCRWIDTH) * GUIDES];
			float dIdx = 0, dIdy = 0;
			for (int i = 0; i < (GUIDES * 3); i++) dIdx += xvalues[i] * xvalues[i], dIdy += yvalues[i] * yvalues[i];
			dHdx[x + y * SCRWIDTH] = sqrtf(1.0f + dIdx);
			dVdy[x + y * SCRWIDTH] = sqrtf(1.0f + dIdy);
		}
	}
	for (int i = 0; i < 2; i++) // two RF iterations
	{
		float sigma_H_i = SIGMA_H * sqrtf(3.0f) * powf(2, (2 - ((float)i + 1))) / sqrtf(15);
		float a = expf(-sqrtf(2.0f) / sigma_H_i);

		for (int y = 0; y < SCRHEIGHT; y++)
		{
			for (int idx = y * SCRWIDTH + 1, x = 1; x < SCRWIDTH; x++, idx++)
			{
				splat[idx] += powf(a, dHdx[idx]) * (splat[idx - 1] - splat[idx]);
			}
			for (int idx = y * SCRWIDTH + SCRWIDTH - 2, x = SCRWIDTH - 2; x >= 0; x--, idx--)
			{
				splat[idx] += powf(a, dHdx[idx + 1]) * (splat[idx + 1] - splat[idx]);
			}
		}
		for (int x = 0; x < SCRWIDTH; x++)
		{
			for (int idx = x + SCRWIDTH, y = 1; y < SCRHEIGHT; y++, idx += SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx]) * (splat[idx - SCRWIDTH] - splat[idx]);
			}
			for (int idx = x + (SCRHEIGHT - 2) * SCRWIDTH, y = SCRHEIGHT - 2; y >= 0; y--, idx -= SCRWIDTH)
			{
				splat[idx] += powf(a, dVdy[idx + SCRWIDTH]) * (splat[idx + SCRWIDTH] - splat[idx]);
			}
		}
	}
}
#endif

void boxFilter(float3* dataIn, float3* dataOut, int kernelSize)
{
	// build SAT
	float3* SAT = new float3[PIXELS];
	for (int y = 0; y < SCRHEIGHT; y++)
	{
		float3 sum(0, 0, 0);
		for (int idx = y * SCRWIDTH, x = 0; x < SCRWIDTH; x++, idx++)
		{
			for (int m = 0; m < SPP; m++) sum += dataIn[idx + m * PIXELS];
			SAT[idx] = sum;
		}
	}
	for (int x = 0; x < SCRWIDTH; x++)
	{
		float3 sum(0, 0, 0);
		for (int idx = x, y = 0; y < SCRHEIGHT; y++, idx += SCRWIDTH) sum += SAT[idx], SAT[idx] = sum;
	}
	// apply filter
	for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++)
	{
		const int x1 = max(0, x - kernelSize - 1), x2 = min(SCRWIDTH - 1, x + kernelSize);
		const int y1 = max(0, y - kernelSize - 1), y2 = min(SCRHEIGHT - 1, y + kernelSize);
		const float3 v = SAT[x2 + y2 * SCRWIDTH] - SAT[x1 + y2 * SCRWIDTH] - SAT[x2 + y1 * SCRWIDTH] + SAT[x1 + y1 * SCRWIDTH];
		dataOut[x + y * SCRWIDTH] = v * (1.0f / (float)((x2 - x1) * (y2 - y1) * SPP));
	}
	// clean up
	delete SAT;
}

void initData()
{
	// load raw samples
	FIBITMAP* bitmap;
	char n[128];
	int idx;
	for (int s = 0; s < SPP; s++)
	{
		// normals
		sprintf(n, "SBMF/images/normal%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		float* data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, N[idx + s * PIXELS] = float3(data[0], data[1], data[2]);
		FreeImage_Unload(bitmap);
		// albedo
		sprintf(n, "SBMF/images/albedo%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, albedo[idx + s * PIXELS] = float3(data[0], data[1], data[2]);
		FreeImage_Unload(bitmap);
		// positions
		sprintf(n, "SBMF/images/position%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, pos[idx + s * PIXELS] = float3(data[0], data[1], data[2]);
		FreeImage_Unload(bitmap);
		// depth
		sprintf(n, "SBMF/images/depth%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, dist[idx + s * PIXELS] = data[0];
		FreeImage_Unload(bitmap);
		// coc
		sprintf(n, "SBMF/images/coc%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, coc[idx + s * PIXELS] = data[0];
		FreeImage_Unload(bitmap);
		// noisy
		sprintf(n, "SBMF/images/noisy%i.exr", s);
		bitmap = FreeImage_Load(FIF_EXR, n);
		data = (float*)FreeImage_GetBits(bitmap);
		for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++, data += 3) idx = x + (SCRHEIGHT - 1 - y) * SCRWIDTH, noisy[idx + s * PIXELS] = float3(data[0], data[1], data[2]);
		FreeImage_Unload(bitmap);
	}
	// boxfilter noisy image
	boxFilter(noisy, boxFiltered, 4);
}

float estimateEigenvector(const float9& X)
{
	float9 randvec;
	float dot = 0;
	for (int i = 0; i < 9; i++) randvec.v[i] = Rand(1) * 0.5f + 0.2f;
	for (int i = 0; i < 9; i++)
	{
		float eigen = 0;
		for (int j = 0; j < 9; j++) eigen += X.v[i] * X.v[j] * randvec.v[j];
		dot += eigen * X.v[i];
	}
	return dot;
}

#if RECURSE_MANIFOLDS_OP == 1
void recurseManifolds(float4* F, float* cluster, int depth)
{
	// add samples to manifold and calculate cluster averages
	float sppReci = 1.0f / SPP;
	memset(manifold, 0, GUIDES * PIXELS * sizeof(float3));
	memset(clusterAvg, 0, PIXELS * sizeof(float));
	for (int i = 0; i < PIXELS; i++) {
#if SPP == 1
		manifold[i * GUIDES + 0] += sppReci * (cluster[i] * N[i]);
		manifold[i * GUIDES + 1] += sppReci * (cluster[i] * pos[i]);
		manifold[i * GUIDES + 2] += sppReci * (cluster[i] * albedo[i]);
		clusterAvg[i] += cluster[i] * sppReci;
#elif SPP == 2
		manifold[i * GUIDES + 0] += sppReci * (cluster[i] * N[i] + cluster[i + PIXELS] * N[i + PIXELS]);
		manifold[i * GUIDES + 1] += sppReci * (cluster[i] * pos[i] + cluster[i + PIXELS] * pos[i + PIXELS]);
		manifold[i * GUIDES + 2] += sppReci * (cluster[i] * albedo[i] + cluster[i + PIXELS] * albedo[i + PIXELS]);
		clusterAvg[i] += (cluster[i] + cluster[i + PIXELS]) * sppReci;
#elif SPP == 3
		manifold[i * GUIDES + 0] += sppReci * (cluster[i] * N[i] + cluster[i + PIXELS] * N[i + PIXELS] + cluster[i + 2 * PIXELS] * N[i + 2 * PIXELS]);
		manifold[i * GUIDES + 1] += sppReci * (cluster[i] * pos[i] + cluster[i + PIXELS] * pos[i + PIXELS] + cluster[i + 2 * PIXELS] * pos[i + 2 * PIXELS]);
		manifold[i * GUIDES + 2] += sppReci * (cluster[i] * albedo[i] + cluster[i + PIXELS] * albedo[i + PIXELS] + cluster[i + 2 * PIXELS] * albedo[i + 2 * PIXELS]);
		clusterAvg[i] += (cluster[i] + cluster[i + PIXELS] + cluster[i + 2 * PIXELS]) * sppReci;
#endif
	}
	// filter manifold values
	h_filter_manifold(SIGMA_H);
	h_filter_clusteravg(SIGMA_H);
	if (depth > 0) for (int i = 0; i < PIXELS; i++)
	{
		float reci = clusterAvg[i] == 0 ? 0.0f : (1.0f / blurredAvg[i]);
		for (int j = 0; j < GUIDES; j++) manifold[i * GUIDES + j] *= reci;
	}
	// splat onto manifold
	float reciSigma = 0.5f * sqrtf(2.0f) / SIGMA_R;
	memset(splat, 0, sizeof(float4) * PIXELS);
	for (int i = 0; i < PIXELS; i++) for (int m = 0; m < SPP; m++)
	{
		float3 dN = (N[i + m * PIXELS] - manifold[i * GUIDES + 0]) * reciSigma;
		float3 dP = (pos[i + m * PIXELS] - manifold[i * GUIDES + 1]) * reciSigma;
		float3 dA = (albedo[i + m * PIXELS] - manifold[i * GUIDES + 2]) * reciSigma;
		float sqrDist = dN.x * dN.x + dN.y * dN.y + dN.z * dN.z + dP.x * dP.x + dP.y * dP.y + dP.z * dP.z + dA.x * dA.x + dA.y * dA.y + dA.z * dA.z;
		if (sqrDist < 64)
		{
			float w = weight[i + m * PIXELS] = expf(-sqrDist);
			splat[i] += float4(noisy[i + m * PIXELS] * w, w) * sppReci;
		}
		else weight[i + m * PIXELS] = 0;
	}

	// blur splats using RF filter
#if TIME_RF
	rf_timer.reset();
#endif
	RF_filter_splats();
#if TIME_RF
	printf("RF_filter_splats: %f\n", rf_timer.elapsed());
#endif

	manifolds++;
	// generate intermediate result
	for (int m = 0; m < SPP; m++) for (int i = 0; i < PIXELS; i++) F[i + m * PIXELS] += splat[i] * weight[i + m * PIXELS];
	// subdivide
	if (depth < MANDEPTH)
	{
		int cIdx0 = 1 + depth * 2, cIdx1 = cIdx0 + 1;
		for (int m = 0; m < SPP; m++) for (int idx = m * PIXELS, i = 0; i < PIXELS; i++, idx++)
		{
			float3 X[3];
			X[0] = N[idx] - manifold[i * GUIDES + 0];
			X[1] = pos[idx] - manifold[i * GUIDES + 1];
			X[2] = albedo[idx] - manifold[i * GUIDES + 2];
			float dot = estimateEigenvector((float9&)X);
			if (dot < 0) clusters[cIdx0][idx] = 1 - weight[idx], clusters[cIdx1][idx] = 0.0f;
			else clusters[cIdx1][idx] = 1 - weight[idx], clusters[cIdx0][idx] = 0.0f;
		}
		// recurse
		recurseManifolds(F, clusters[cIdx0], depth + 1);
		recurseManifolds(F, clusters[cIdx1], depth + 1);
	}

}

#else
void recurseManifolds(float4* F, float* cluster, int depth)
{
	// add samples to manifold and calculate cluster averages
	float sppReci = 1.0f / SPP;
	memset(manifold, 0, GUIDES * PIXELS * sizeof(float3));
	memset(clusterAvg, 0, PIXELS * sizeof(float));
	for (int i = 0; i < PIXELS; i++)
		for (int m = 0; m < SPP; m++) {
			{
				manifold[i * GUIDES + 0] += (sppReci * cluster[i + m * PIXELS]) * N[i + m * PIXELS];
				manifold[i * GUIDES + 1] += (sppReci * cluster[i + m * PIXELS]) * pos[i + m * PIXELS];
				manifold[i * GUIDES + 2] += (sppReci * cluster[i + m * PIXELS]) * albedo[i + m * PIXELS];
				clusterAvg[i] += cluster[i + m * PIXELS] * sppReci;
			}
		}
	// filter manifold values
	h_filter_manifold(SIGMA_H);
	h_filter_clusteravg(SIGMA_H);
	if (depth > 0) for (int i = 0; i < PIXELS; i++)
	{
		float reci = clusterAvg[i] == 0 ? 0.0f : (1.0f / blurredAvg[i]);
		for (int j = 0; j < GUIDES; j++) manifold[i * GUIDES + j] *= reci;
	}
	// splat onto manifold
	float reciSigma = 0.5f * sqrtf(2.0f) / SIGMA_R;
	memset(splat, 0, sizeof(float4) * PIXELS);
	for (int i = 0; i < PIXELS; i++) for (int m = 0; m < SPP; m++)
	{
		float3 dN = (N[i + m * PIXELS] - manifold[i * GUIDES + 0]) * reciSigma;
		float3 dP = (pos[i + m * PIXELS] - manifold[i * GUIDES + 1]) * reciSigma;
		float3 dA = (albedo[i + m * PIXELS] - manifold[i * GUIDES + 2]) * reciSigma;
		float sqrDist = dN.x * dN.x + dN.y * dN.y + dN.z * dN.z + dP.x * dP.x + dP.y * dP.y + dP.z * dP.z + dA.x * dA.x + dA.y * dA.y + dA.z * dA.z;
		if (sqrDist < 64)
		{
			float w = weight[i + m * PIXELS] = expf(-sqrDist);
			splat[i] += float4(noisy[i + m * PIXELS] * w, w) * sppReci;
		}
		else weight[i + m * PIXELS] = 0;
	}

	// blur splats using RF filter
#if TIME_RF
	rf_timer.reset();
#endif
	RF_filter_splats();
#if TIME_RF
	printf("RF_filter_splats: %f\n", rf_timer.elapsed());
#endif

	manifolds++;
	// generate intermediate result
	for (int m = 0; m < SPP; m++) for (int i = 0; i < PIXELS; i++) F[i + m * PIXELS] += splat[i] * weight[i + m * PIXELS];
	// subdivide
	if (depth < MANDEPTH)
	{
		int cIdx0 = 1 + depth * 2, cIdx1 = cIdx0 + 1;
		for (int m = 0; m < SPP; m++) for (int idx = m * PIXELS, i = 0; i < PIXELS; i++, idx++)
		{
			float3 X[3];
			X[0] = N[idx] - manifold[i * GUIDES + 0];
			X[1] = pos[idx] - manifold[i * GUIDES + 1];
			X[2] = albedo[idx] - manifold[i * GUIDES + 2];
			float dot = estimateEigenvector((float9&)X);
			if (dot < 0) clusters[cIdx0][idx] = 1 - weight[idx], clusters[cIdx1][idx] = 0.0f;
			else clusters[cIdx1][idx] = 1 - weight[idx], clusters[cIdx0][idx] = 0.0f;
		}
		// recurse
		recurseManifolds(F, clusters[cIdx0], depth + 1);
		recurseManifolds(F, clusters[cIdx1], depth + 1);
	}

}

#endif // RECURSE_MANIFOLDS_OP == 1

void adaptiveManifolds()
{
	// build manifold tree
	recurseManifolds(F, clusters[0], 0);
	// finalize
	for (int m = 0; m < SPP; m++) for (int i = 0; i < PIXELS; i++)
	{
		int idx = i + m * PIXELS;
		float weight = F[idx].w;
		// float3 normalized = (weight == 0) ? boxFiltered[i] : F[idx].xyz * (1.0f / weight);
		float3 normalized = (weight < OUTLIERTHRESHOLD) ? boxFiltered[i] : (F[idx].xyz * (1.0f / weight));
		F[i + m * PIXELS] = float4(normalized, 0);
	}
}

float4 getBoxFilteredSample(float4* SAT, int x, int y, int kernelSize)
{
	const int x1 = max(0, x - kernelSize - 1), x2 = min(SCRWIDTH - 1, x + kernelSize);
	const int y1 = max(0, y - kernelSize - 1), y2 = min(SCRHEIGHT - 1, y + kernelSize);
	const float4 v = SAT[x2 + y2 * SCRWIDTH] - SAT[x1 + y2 * SCRWIDTH] - SAT[x2 + y1 * SCRWIDTH] + SAT[x1 + y1 * SCRWIDTH];
	return v * (1.0f / (float)((x2 - x1) * (y2 - y1) * SPP));
}

void linearManifolds(float4* F, float* depth, float* coc, float focalDist, int numRad, float radius)
{
	// compute scales of the manifolds
	int mrad[15];
	for (int i = 0; i < numRad; i++) mrad[i] = (int)((((float)i + 1) / numRad) * radius); // for radius = 10, numRad = 3: { 3, 6, 10 }
	int numManifolds = numRad * 2 + 1; // for numRad = 3 : numManifolds = 7
									   // allocate and initialize linear manifolds
	lmanifold = new float4*[numManifolds];
	coverage = new float*[numRad + 1];
	for (int i = 0; i < numManifolds; i++)
	{
		lmanifold[i] = new float4[PIXELS];
		if (i < (numRad + 1))
		{
			coverage[i] = new float[PIXELS];
			memset(coverage[i], 0, PIXELS * sizeof(float));
		}
		memset(lmanifold[i], 0, PIXELS * sizeof(float4));
	}
	// assign samples to linear manifolds
	for (int s = 0; s < SPP; s++) for (int i = 0; i < PIXELS; i++)
	{
		float C = coc[i + s * PIXELS];
		float ls = min(1.0f, max(0.0f, C / radius));
		if (depth[i + s * PIXELS] < focalDist) ls = -ls;
		// compute neighboring manifold indices and blend factor
		float mls = ls * 0.5f + 0.5f;
		int m0 = min(numManifolds - 1, max(0, (int)(mls * (float)(numManifolds - 1))));
		int m1 = min(numManifolds - 1, m0 + 1);
		float blend = mls * (float)(numManifolds - 1) - (float)m0;
		// compute the contribution of the current sample
		float4 contrib(F[i + s * PIXELS].xyz, 1.0f);
		contrib *= 1.0f / SPP;
		// add contributions to manifolds
		lmanifold[m0][i] += contrib * (1.0f - blend);
		lmanifold[m1][i] += contrib * blend;
	}
	// build SATs for the manifolds
	manSAT = new float4*[numManifolds];
	for (int i = 0; i < numManifolds; i++)
	{
		// build SAT
		manSAT[i] = new float4[PIXELS];
		for (int y = 0; y < SCRHEIGHT; y++)
		{
			float4 sum(0, 0, 0, 0);
			for (int idx = y * SCRWIDTH, x = 0; x < SCRWIDTH; x++, idx++) sum += lmanifold[i][idx], manSAT[i][idx] = sum;
		}
		for (int x = 0; x < SCRWIDTH; x++)
		{
			float4 sum(0, 0, 0, 0);
			for (int idx = x, y = 0; y < SCRHEIGHT; y++, idx += SCRWIDTH) sum += manSAT[i][idx], manSAT[i][idx] = sum;
		}
	}
	// accumulate manifold contribution
	memset(LF, 0, PIXELS * sizeof(float4));
	for (int i = 0; i < numManifolds; i++)
	{
		printf("linear manifold %i...\n", i);
		int scale = abs(i - numRad);
		// compute weighting for current layer
		for (int j = 0; j < PIXELS; j++) currentw[j] = 1.0f;
		for (int k = 0; k < (numRad + 1); k++) if (k != scale) for (int j = 0; j < PIXELS; j++) currentw[j] -= coverage[k][j];
		// accumulate contribution of current manifold
		for (int k = 0; k < (numRad + 1); k++)
		{
			if (k == scale)
			{
				for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++)
				{
					int idx = x + y * SCRWIDTH;
					float4 boxSample = k == 0 ? lmanifold[i][idx] : getBoxFilteredSample(manSAT[i], x, y, mrad[k - 1]);
					LF[idx] += boxSample * currentw[idx];
					coverage[k][idx] += boxSample.w * currentw[idx];
				}
			}
			else
			{
				for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++)
				{
					int idx = x + y * SCRWIDTH;
					float4 boxSample = k == 0 ? lmanifold[i][idx] : getBoxFilteredSample(manSAT[i], x, y, mrad[k - 1]);
					LF[idx] += boxSample * coverage[k][idx];
				}
			}
		}
	}
	// normalize output
	for (int j = 0; j < PIXELS; j++)
	{
		LF[j] *= 1.0f / LF[j].w;
		LF[j].x = max(0.0f, LF[j].x);
		LF[j].y = max(0.0f, LF[j].y);
		LF[j].z = max(0.0f, LF[j].z);
	}
}

void Game::Init()
{
	// initialize timers
	init_timer.init();
	rf_timer.init();
	lm_timer.init();
	tot_timer.init();

#if RF_GPGPU == 1
	rf_y_func = new Kernel("program.cl", "RFFilterSplatsY");
	rf_x_func = new Kernel("program.cl", "RFFilterSplatsX");

	bool initRfy = rf_y_func->InitCL();
	bool initRfx = rf_x_func->InitCL();

	printf("initRfy: %i\n", initRfy);
	printf("initRfx: %i\n", initRfx);

#endif


#if TIME_TOT
	tot_timer.reset();
#endif

	// fill arrays N, pos, albedo, noisy
#if TIME_TOT
	init_timer.reset();
#endif
	initData();
#if TIME_LM
	printf("initData: %f\n", init_timer.elapsed());
#endif
	// initialize arrays
	memset(F, 0, PIXELS * SPP * sizeof(float4));
	memset(result, 0, PIXELS * sizeof(float3));
	for (int i = 0; i < 9; i++) clusters[i] = new float[PIXELS * SPP];
	for (int i = 0; i < PIXELS * SPP; i++) clusters[0][i] = 1.0f;
	// construct adaptive manifolds
	adaptiveManifolds();
	printf("generated %i adaptive manifolds.\n", manifolds);
	// construct linear manifolds
	float focalDist = 3.5f;
	float radius = 8.0f;

#if TIME_LM
	lm_timer.reset();
#endif
	linearManifolds(F, dist, coc, focalDist, 3, radius);
#if TIME_LM
	printf("linearManifolds: %f\n", lm_timer.elapsed());
#endif

	// finalize
	for (int i = 0; i < PIXELS; i++) result[i] = LF[i].xyz;

#if TIME_TOT
	printf("total time: %f\n", tot_timer.elapsed());
#endif

}

void Game::Tick(float a_DT)
{
	// visualize data
	uint* dst = (uint*)m_Screen->GetBuffer();
	float scale = 1.0f / SPP;
	for (int y = 0; y < SCRHEIGHT; y++) for (int x = 0; x < SCRWIDTH; x++)
	{
		float r = 0, g = 0, b = 0;
		if (y < my)
		{
			float r, g, b;
			if (x < mx)
			{
				r = manifold[(x + y * SCRWIDTH) * 3 + 0].x;
				g = manifold[(x + y * SCRWIDTH) * 3 + 0].y;
				b = manifold[(x + y * SCRWIDTH) * 3 + 0].z;
			}
			else
			{
				r = noisy[x + y * SCRWIDTH].x * 2.0f;
				g = noisy[x + y * SCRWIDTH].y * 2.0f;
				b = noisy[x + y * SCRWIDTH].z * 2.0f;
			}
			int ir = (int)(255.0f * min(1.0f, sqrtf(r)));
			int ig = (int)(255.0f * min(1.0f, sqrtf(g)));
			int ib = (int)(255.0f * min(1.0f, sqrtf(b)));
			*dst++ = (ir << 16) + (ig << 8) + ib;
		}
		else
		{
			r = result[x + y * SCRWIDTH].x * 2.0f;
			g = result[x + y * SCRWIDTH].y * 2.0f;
			b = result[x + y * SCRWIDTH].z * 2.0f;
			int ir = (int)(255.0f * min(1.0f, sqrtf(r)));
			int ig = (int)(255.0f * min(1.0f, sqrtf(g)));
			int ib = (int)(255.0f * min(1.0f, sqrtf(b)));
			*dst++ = (ir << 16) + (ig << 8) + ib;
		}
	}
	char t[256];
	sprintf(t, "x: %i", mx);
	m_Screen->Print(t, 2, 2, 0xffffff);
	sprintf(t, "y: %i", my);
	m_Screen->Print(t, 2, 12, 0xffffff);
}