// Template, major revision 6, update for INFOMOV
// IGAD/NHTV/UU - Jacco Bikker - 2006-2016

#pragma once

#include "math.h"
#include "stdlib.h"
#include "emmintrin.h"
#include "immintrin.h"
#include <xmmintrin.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include "stdio.h"
#include "windows.h"
#include "surface.h"
#include "game.h"
#include <vector>
#include "freeimage.h"
#include "threads.h"

#define USE_GLEE
#define FATALERROR(m) FatalError( __FILE__, __LINE__, m )
#define ERRORMESSAGE(m,c) FatalError( __FILE__, __LINE__, m, c )
#define CHECKCL(r) CheckCL( r, __FILE__, __LINE__ )

HWND GetWindowHandle();
void FatalError( const char* file, int line, const char* message );
void FatalError( const char* file, int line, const char* message, const char* context );

// system includes
#include <cl/cl.h>
#include <cl/cl_gl_ext.h>
#include <GL/glee.h>
#include <string>
#include "io.h"
#include <ios>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "SDL.h"
#include "SDL_syswm.h"
#include "fcntl.h"

using namespace Tmpl8;				// to use template classes
using namespace std;				// to use stl vectors

inline float Rand( float range ) { return ((float)rand() / RAND_MAX) * range; }
inline int IRand( int range ) { return rand() % range; }
int filesize( FILE* f );

namespace Tmpl8 {

#define PI					3.14159265358979323846264338327950f
#define INVPI				0.31830988618379067153776752674503f

#define MALLOC64(x)			_aligned_malloc(x,64)
#define FREE64(x)			_aligned_free(x)
#define PREFETCH(x)			_mm_prefetch((const char*)(x),_MM_HINT_T0)
#define PREFETCH_ONCE(x)	_mm_prefetch((const char*)(x),_MM_HINT_NTA)
#define PREFETCH_WRITE(x)	_m_prefetchw((const char*)(x))
#define loadss(mem)			_mm_load_ss((const float*const)(mem))
#define broadcastps(ps)		_mm_shuffle_ps((ps),(ps), 0)
#define broadcastss(ss)		broadcastps(loadss((ss)))

struct Timer 
{ 
	typedef long long value_type; 
	static double inv_freq; 
	value_type start; 
	Timer() : start( get() ) { init(); } 
	float elapsed() const { return (float)((get() - start) * inv_freq); } 
	static value_type get() 
	{ 
		LARGE_INTEGER c; 
		QueryPerformanceCounter( &c ); 
		return c.QuadPart; 
	} 
	static double to_time(const value_type vt) { return double(vt) * inv_freq; } 
	void reset() { start = get(); }
	static void init() 
	{ 
		LARGE_INTEGER f; 
		QueryPerformanceFrequency( &f ); 
		inv_freq = 1000./double(f.QuadPart); 
	} 
}; 

typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned char byte;

#define BADFLOAT(x) ((*(uint*)&x & 0x7f000000) == 0x7f000000)

typedef unsigned char byte;
class int2;
class int3;
class float2;
class float3;

class int2
{
public:
	union
	{
		struct { int x, y; };
		int cell[2];
	};
	int2() {}
	int2( const int _X, const int _Y) : x( _X ), y( _Y ) {}

	inline int& operator[] ( unsigned int i ) { return cell[i]; }
	// vector operators
	inline void operator += ( int2 a ) { x +=  a.x; y +=  a.y; }
	inline void operator -= ( int2 a ) { x -= a.x; y -= a.y; }
	inline void operator *= ( int2 a ) { x *= a.x; y *= a.y; }
	inline void operator /= ( int2 a ) { x /= a.x; y /= a.y; }
	// int operators
	inline void operator *= ( int a ) { x *= a; y *= a; }
	inline void operator /= ( int a ) { x /= a; y /= a; }
	// vector operators
	inline int2 operator + ( const int2& a ) { return int2( x + a.x, y + a.y ); }
	inline int2 operator - ( const int2& a ) { return int2( x - a.x, y - a.y ); }
	inline int2 operator * ( const int2& a ) { return int2( x * a.x, y * a.y ); }
	inline int2 operator / ( const int2& a ) { return int2( x / a.x, y / a.y ); }
	// int operators
	inline int2 operator * ( const int& a ) { return int2( x * a, y * a ); }
	inline int2 operator / ( const int& a ) { return int2( x / a, y / a ); }

	bool operator == ( const int2& a ) { return a.x == x && a.y == y; }
	bool operator != ( const int2& a ) { return a.x != x || a.y != y; }
};

class int3
{
public:
	union
	{
		struct { int x, y, z; };
		int cell[3];
	};
	int3() {}
	int3( const int _X, const int _Y, const int _Z) : x( _X ), y( _Y ), z( _Z ) {}

	inline int&operator[] ( int i ) { return cell[i]; }
	// vector operators
	inline void operator += ( int3 a ) { x += a.x; y += a.y; z += a.z; }
	inline void operator -= ( int3 a ) { x -= a.x; y -= a.y; z -= a.z; }
	inline void operator *= ( int3 a ) { x *= a.x; y *= a.y; z *= a.z; }
	inline void operator /= ( int3 a ) { x /= a.x; y /= a.y; z /= a.z; }
	// int operators
	inline void operator *= ( int a ) { x *= a; y *= a; z *= a; }
	inline void operator /= ( int a ) { x /= a; y /= a; z /= a; }
	// vector operators
	inline int3 operator + ( const int3& a ) { return int3( x + a.x, y + a.y, z + a.z ); }
	inline int3 operator - ( const int3& a ) { return int3( x - a.x, y - a.y, z - a.z ); }
	inline int3 operator * ( const int3& a ) { return int3( x * a.x, y * a.y, z * a.y ); }
	inline int3 operator / ( const int3& a ) { return int3( x / a.x, y / a.y, z / a.z ); }
	// int operators
	inline int3 operator * ( const int& a ) { return int3( x * a, y * a, z * a ); }
	inline int3 operator / ( const int& a ) { return int3( x / a, y / a, z / a ); }

	bool operator == ( const int3& a ) { return a.x == x && a.y == y && a.z == z; }
	bool operator != ( const int3& a ) { return a.x != x || a.y != y || a.z != z; }
};

class float2
{
public:
	union
	{
		struct { float x, y; };
		float cell[2];
	};
	float2() {}
	float2( const float _X, const float _Y ) : x( _X ), y( _Y ) {}
	float2(int2 a) : x((float)a.x), y((float)a.y){}

	inline float  operator[] ( int i ) const { return cell[i]; }
	inline float& operator[] ( int i ) 		 { return cell[i]; }
	inline float2 operator - (		 ) 		 { return float2( -x, -y ); }

	friend inline float2 operator + ( const float2&a, const float2&b ){ return float2(a.x + b.x, a.y + b.y); }
	friend inline float2 operator - ( const float2&a, const float2&b ){ return float2(a.x - b.x, a.y - b.y); }
	friend inline float2 operator * ( const float2&a, const float2&b ){ return float2(a.x * b.x, a.y * b.y); }
	friend inline float2 operator / ( const float2&a, const float2&b ){ return float2(a.x / b.x, a.y / b.y); }
	friend inline float2 operator + ( const float2&a, const float &b ){ return float2(a.x + b  , a.y + b  ); }
	friend inline float2 operator - ( const float2&a, const float &b ){ return float2(a.x - b  , a.y - b  ); }
	friend inline float2 operator * ( const float2&a, const float &b ){ return float2(a.x * b  , a.y * b  ); }
	friend inline float2 operator / ( const float2&a, const float &b ){ return float2(a.x / b  , a.y / b  ); }
	friend inline float2 operator * ( const float &a, const float2&b ){ return float2(a   * b.x, a   * b.y); }
	friend inline float2 operator / ( const float &a, const float2&b ){ return float2(a   / b.x, a   / b.y); }

	// vector operators
	inline void operator += ( const float2 a ) { *this = *this + a; }
	inline void operator -= ( const float2 a ) { *this = *this - a; }
	inline void operator *= ( const float2 a ) { *this = *this * a; }
	inline void operator /= ( const float2 a ) { *this = *this / a; }
	inline void operator *= ( const float  a ) { *this = *this * a; }
	inline void operator /= ( const float  a ) { *this = *this / a; }

	bool operator == ( const float2& a ) { return a.x == x && a.y == y; }
	bool operator != ( const float2& a ) { return a.x != x || a.y != y; }
};

class float3
{
public:
	union
	{
		struct { float2 xy; };
		struct { float x, y, z; };
		float cell[3];
	};
	float3() {}
	float3( const float2 _V, const float _Z ) : xy( _V ), z( _Z ) {}
	float3( const float _X, const float _Y, const float _Z ) : x( _X ), y( _Y ), z( _Z ) {}

	inline float  operator[] ( int i ) const { return cell[i]; }
	inline float& operator[] ( int i ) 		 { return cell[i]; }
	inline float3 operator - (		 ) 		 { return float3( -x, -y, -z ); }

	friend inline float3 operator + ( const float3&a, const float3&b ){ return float3(a.x + b.x, a.y + b.y, a.z + b.z); }
	friend inline float3 operator - ( const float3&a, const float3&b ){ return float3(a.x - b.x, a.y - b.y, a.z - b.z); }
	friend inline float3 operator * ( const float3&a, const float3&b ){ return float3(a.x * b.x, a.y * b.y, a.z * b.z); }
	friend inline float3 operator / ( const float3&a, const float3&b ){ return float3(a.x / b.x, a.y / b.y, a.z / b.z); }
	friend inline float3 operator * ( const float3&a, const float &b ){ return float3(a.x * b  , a.y * b  , a.z * b  ); }
	friend inline float3 operator / ( const float3&a, const float &b ){ return float3(a.x / b  , a.y / b  , a.z / b  ); }
	friend inline float3 operator * ( const float &a, const float3&b ){ return float3(a   * b.x, a   * b.y, a   * b.z); }
	friend inline float3 operator / ( const float &a, const float3&b ){ return float3(a   / b.x, a   / b.y, a   / b.z); }

	// vector operators
	inline void operator += ( const float3 a ) { *this = *this + a; }
	inline void operator -= ( const float3 a ) { *this = *this - a; }
	inline void operator *= ( const float3 a ) { *this = *this * a; }
	inline void operator /= ( const float3 a ) { *this = *this / a; }
	inline void operator *= ( const float  a ) { *this = *this * a; }
	inline void operator /= ( const float  a ) { *this = *this / a; }

	bool operator == ( const float3& a ) { return a.x == x && a.y == y && a.z == z; }
	bool operator != ( const float3& a ) { return a.x != x || a.y != y || a.z != z; }

	
	static float3 Lerp(const float3&a, const float3&b, float t) { return a * (1.0f - t) + b * t; }

};

//aligned float3 ( for SIMD )
__declspec(align(16))
class float3a
{
public:
	union
	{
		__m128 quad;
		struct { float2 xy; };
		struct { float x, y, z, w; };
		float cell[4];
	};
	float3a() {}
	float3a( const float2 _V, const float _Z ) : xy( _V ), z( _Z ) {}
	float3a( const float _X, const float _Y, const float _Z ) : x( _X ), y( _Y ), z( _Z ) {}
	float3a( const float3 _V ) : x( _V.x ), y( _V.y ), z( _V.z ) {}

	inline float   operator[] ( int i ) const { return cell[i]; }
	inline float&  operator[] ( int i ) 	  { return cell[i]; }
	inline float3a operator - (		 ) 		  { return float3a( -x, -y, -z ); }

	friend inline float3a operator + ( const float3a&a, const float3a&b ){ return float3a(a.x + b.x, a.y + b.y, a.z + b.z); }
	friend inline float3a operator - ( const float3a&a, const float3a&b ){ return float3a(a.x - b.x, a.y - b.y, a.z - b.z); }
	friend inline float3a operator * ( const float3a&a, const float3a&b ){ return float3a(a.x * b.x, a.y * b.y, a.z * b.z); }
	friend inline float3a operator / ( const float3a&a, const float3a&b ){ return float3a(a.x / b.x, a.y / b.y, a.z / b.z); }
	friend inline float3a operator * ( const float3a&a, const float  &b ){ return float3a(a.x * b  , a.y * b  , a.z * b  ); }
	friend inline float3a operator / ( const float3a&a, const float  &b ){ return float3a(a.x / b  , a.y / b  , a.z / b  ); }
	friend inline float3a operator * ( const float  &a, const float3a&b ){ return float3a(a   * b.x, a   * b.y, a   * b.z); }
	friend inline float3a operator / ( const float  &a, const float3a&b ){ return float3a(a   / b.x, a   / b.y, a   / b.z); }

	// vector operators
	inline void operator += ( const float3a&a ) { *this = *this + a; }
	inline void operator -= ( const float3a&a ) { *this = *this - a; }
	inline void operator *= ( const float3a&a ) { *this = *this * a; }
	inline void operator /= ( const float3a&a ) { *this = *this / a; }
	inline void operator *= ( const float   a ) { *this = *this * a; }
	inline void operator /= ( const float   a ) { *this = *this / a; }
};

__declspec(align(16))
class float4
{
public:
	union
	{
		__m128 quad;
		struct{ float2 xy; float2 zw; };
		struct{ float3 xyz; };
		struct{ float x, y, z, w; };
		float cell[4];
	};
	float4() {}
	float4( const float2 _XY, const float2 _ZW ) : xy( _XY ), zw( _ZW ) {}
	float4( const float3 _XYZ, const float _W ) : xyz( _XYZ ), w( _W ) {}
	float4( const float _X, const float _Y, const float _Z, const float _W) : x( _X ), y( _Y ), z( _Z ), w( _W ) {}

	inline float  operator[] ( int i ) const { return cell[i]; }
	inline float& operator[] ( int i ) 		 { return cell[i]; }
	inline float4 operator - (		 ) 		  { return float4( -x, -y, -z, -w ); }

	friend inline float4 operator + ( const float4&a, const float4&b ){ return float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w); }
	friend inline float4 operator - ( const float4&a, const float4&b ){ return float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w); }
	friend inline float4 operator * ( const float4&a, const float4&b ){ return float4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w); }
	friend inline float4 operator / ( const float4&a, const float4&b ){ return float4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w); }
	friend inline float4 operator * ( const float4&a, const float &b ){ return float4(a.x * b  , a.y * b  , a.z * b  , a.w * b  ); }
	friend inline float4 operator / ( const float4&a, const float &b ){ return float4(a.x / b  , a.y / b  , a.z / b  , a.w / b  ); }
	friend inline float4 operator * ( const float &a, const float4&b ){ return float4(a   * b.x, a   * b.y, a   * b.z, a   * b.w); }
	friend inline float4 operator / ( const float &a, const float4&b ){ return float4(a   / b.x, a   / b.y, a   / b.z, a   / b.w); }

	// vector operators
	inline void operator += ( const float4&a ) { *this = *this + a; }
	inline void operator -= ( const float4&a ) { *this = *this - a; }
	inline void operator *= ( const float4&a ) { *this = *this * a; }
	inline void operator /= ( const float4&a ) { *this = *this / a; }
	inline void operator *= ( const float  a ) { *this = *this * a; }
	inline void operator /= ( const float  a ) { *this = *this / a; }
	
	bool operator == ( const float4& a ) const { return a.x == x && a.y == y && a.z == z && a.w == w; }
	bool operator != ( const float4& a ) const { return a.x != x || a.y != y || a.z != z || a.z != z; }
};

// reciprocal
static inline float2 Rcp( const float2& a ) { return float2( 1 / a.x, 1 / a.y ); }
static inline float3 Rcp( const float3& a ) { return float3( 1 / a.x, 1 / a.y, 1 / a.z ); }
static inline float4 Rcp( const float4& a ) { return float4( 1 / a.x, 1 / a.y, 1 / a.z, 1 / a.w ); }
// dot product
static inline float Dot( const float2& a, const float2& b ) { return a.x * b.x + a.y * b.y; }
static inline float Dot( const float3& a, const float3& b ) { return a.x * b.x + a.y * b.y + a.z * b.z; }
static inline float Dot( const float4& a, const float4& b ) { return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w; }
// cross product
static inline float  Cross( const float2& a, const float2& b ) { return a.x * b.y - a.y * b.x; }
static inline float3 Cross( const float3& a, const float3& b ) { return float3( a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x ); }
// vector length
static inline float Length( const float2& _Vec ) { return sqrtf( Dot( _Vec, _Vec ) ); }
static inline float Length( const float3& _Vec ) { return sqrtf( Dot( _Vec, _Vec ) ); }
static inline float Length( const float4& _Vec ) { return sqrtf( Dot( _Vec, _Vec ) ); }
static inline float LengthSquare( const float2& _Vec ) { return Dot( _Vec, _Vec ); }
static inline float LengthSquare( const float3& _Vec ) { return Dot( _Vec, _Vec ); }
static inline float LengthSquare( const float4& _Vec ) { return Dot( _Vec, _Vec ); }
// vector normalize
static inline float2 Normalize( float2& _Vec ) { return _Vec / Length( _Vec ); }
static inline float3 Normalize( float3& _Vec ) { return _Vec / Length( _Vec ); }
static inline float4 Normalize( float4& _Vec ) { return _Vec / Length( _Vec ); }

class Quaternion // not checked!
{
public:
	float x,y,z,w;
	Quaternion(){}
	Quaternion(float _x,float _y,float _z,float _w):x(_x),y(_y),z(_z),w(_w)
	{
		
	}
	static Quaternion AxisAngle(float3 _Axis, float _Angle)
	{
		Quaternion r;
		float result = sin(_Angle/2.0f);
		r.w = cosf(_Angle/2.0f);
		r.x = _Axis.x * result;
		r.y = _Axis.y * result;
		r.z = _Axis.z * result;
		return r;
	}
	Quaternion Quaternion::operator *(const Quaternion &q)
	{
		Quaternion r;
		r.w = w * q.w - x * q.x - y * q.y - z * q.z;
		r.x = w * q.x + x * q.w + y * q.z - z * q.y;
		r.y = w * q.y + y * q.w + z * q.x - x * q.z;
		r.z = w * q.z + z * q.w + x * q.y - y * q.x;
		return r;
	}

	void operator *= ( const Quaternion &q ) { *this = *this * q; }
};

class mat4
{
public:
	union
	{
		struct { float4 row[4]; };
		struct { float4 x, y, z, w; };
	};
	mat4()
	{
		x = float4( 1, 0, 0, 0 );
		y = float4( 0, 1, 0, 0 );
		z = float4( 0, 0, 1, 0 );
		w = float4( 0, 0, 0, 1 );
	}
	mat4( float4&x, float4&y, float4&z, float4&w ) { x = x; y = y; z = z; w = w; }
	mat4( float xx, float xy, float xz, float xw,
		float yx, float yy, float yz, float yw,
		float zx, float zy, float zz, float zw,
		float wx, float wy, float wz, float ww )
	{
		x = float4( xx, xy, xz, xw );
		y = float4( yx, yy, yz, yw );
		z = float4( zx, zy, zz, zw );
		w = float4( wx, wy, wz, ww );
	}
	mat4( const Quaternion&_Q)
	{
		x = float4(1.0f - 2.0f * ( _Q.y * _Q.y + _Q.z * _Q.z ), 2.0f * ( _Q.x * _Q.y - _Q.w * _Q.z ), 2.0f * ( _Q.x * _Q.z + _Q.w * _Q.y ), 0.0f);
		y = float4(2.0f * ( _Q.x * _Q.y + _Q.w * _Q.z ), 1.0f - 2.0f * ( _Q.x * _Q.x + _Q.z * _Q.z ), 2.0f * ( _Q.y * _Q.z - _Q.w * _Q.x ), 0.0f);
		z = float4(2.0f * ( _Q.x * _Q.z - _Q.w * _Q.y ), 2.0f * ( _Q.y * _Q.z + _Q.w * _Q.x ), 1.0f - 2.0f * ( _Q.x * _Q.x + _Q.y * _Q.y ), 0.0f);
		w = float4(0, 0, 0, 1);
	}

	bool operator == ( const mat4& a ) { return a.x == x && a.y == y && a.z == z && a.w == w; }
	bool operator != ( const mat4& a ) { return a.x != x || a.y != y || a.z != z || a.w != w; }

	inline float4  operator[] ( int i ) const	{ return row[i]; }
	inline float4& operator[] ( int i ) 		{ return row[i]; }

	void operator *= ( const mat4& b )
	{
		mat4 a = *this;
		*this = a * b;
	}

	inline mat4 operator * ( const mat4& a ) const
	{
		mat4 b = *this;
		return mat4(
			Dot( b.x, float4( a.row[0][0], a.row[1][0], a.row[2][0], a.row[3][0] ) ),
			Dot( b.x, float4( a.row[0][1], a.row[1][1], a.row[2][1], a.row[3][1] ) ),
			Dot( b.x, float4( a.row[0][2], a.row[1][2], a.row[2][2], a.row[3][2] ) ),
			Dot( b.x, float4( a.row[0][3], a.row[1][3], a.row[2][3], a.row[3][3] ) ),

			Dot( b.y, float4( a.row[0][0], a.row[1][0], a.row[2][0], a.row[3][0] ) ),
			Dot( b.y, float4( a.row[0][1], a.row[1][1], a.row[2][1], a.row[3][1] ) ),
			Dot( b.y, float4( a.row[0][2], a.row[1][2], a.row[2][2], a.row[3][2] ) ),
			Dot( b.y, float4( a.row[0][3], a.row[1][3], a.row[2][3], a.row[3][3] ) ),

			Dot( b.z, float4( a.row[0][0], a.row[1][0], a.row[2][0], a.row[3][0] ) ),
			Dot( b.z, float4( a.row[0][1], a.row[1][1], a.row[2][1], a.row[3][1] ) ),
			Dot( b.z, float4( a.row[0][2], a.row[1][2], a.row[2][2], a.row[3][2] ) ),
			Dot( b.z, float4( a.row[0][3], a.row[1][3], a.row[2][3], a.row[3][3] ) ),

			Dot( b.w, float4( a.row[0][0], a.row[1][0], a.row[2][0], a.row[3][0] ) ),
			Dot( b.w, float4( a.row[0][1], a.row[1][1], a.row[2][1], a.row[3][1] ) ),
			Dot( b.w, float4( a.row[0][2], a.row[1][2], a.row[2][2], a.row[3][2] ) ),
			Dot( b.w, float4( a.row[0][3], a.row[1][3], a.row[2][3], a.row[3][3] ) )
			);
	}
	inline float4 operator * ( const float4& a ) const
	{
		return float4(
			Dot( a, float4( row[0][0], row[1][0], row[2][0], row[3][0] ) ),
			Dot( a, float4( row[0][1], row[1][1], row[2][1], row[3][1] ) ),
			Dot( a, float4( row[0][2], row[1][2], row[2][2], row[3][2] ) ),
			Dot( a, float4( row[0][3], row[1][3], row[2][3], row[3][3] ) )
			);
	}
	inline float3 operator * ( const float3& a ) const
	{
		return float3(
			Dot( a, float3( row[0][0], row[1][0], row[2][0] ) ) + row[3][0], 
			Dot( a, float3( row[0][1], row[1][1], row[2][1] ) ) + row[3][1],
			Dot( a, float3( row[0][2], row[1][2], row[2][2] ) ) + row[3][2] );
	}
	inline mat4 operator * ( const float a ) const
	{
		mat4 b;
		b = *this;
		b.x *= a;
		b.y *= a;
		b.z *= a;
		b.w *= a;
		return b;
	}

	static mat4 LookAt(const float3 _From, const float3 _To, const float3 _Up)
	{
		float3 forward = Normalize(_To-_From);
		float3 side = Normalize(Cross(forward,_Up));
		float3 up = Normalize(Cross(side,forward));
		mat4 mat=mat4::Identity();
		mat.x.xyz = -side;		//not sure why i need a negation
		mat.y.xyz = up;
		mat.z.xyz = forward;
		mat.w.xyz = _From;
		return mat;
	}

	static mat4 Lerp(const mat4&a, const mat4&b, float t)
	{
		float t2 = 1.0f - t;
		mat4 r;
		for(int i = 0; i < 4; i++)
			r.row[i] = a.row[i] * t2 + b.row[i] * t;
		return r;
	}

	static mat4 RotateX( float a )
	{
		float x = cosf( a );
		float y = sinf( a );
		return mat4(1, 0, 0, 0,	0, x, y, 0,	0, -y, x, 0, 0, 0, 0, 1 );
	}
	static mat4 RotateY( float a )
	{
		float x = cosf( a );
		float y = sinf( a );
		return mat4( x, 0, -y, 0, 0, 1, 0, 0, y, 0, x, 0, 0, 0, 0, 1 );
	}
	static mat4 RotateZ( float a )
	{
		float x = cosf( a );
		float y = sinf( a );
		return mat4( x, y, 0, 0, -y, x, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 );
	}
	static mat4 Rotate( float3 _XYZ )
	{
		return RotateX(_XYZ.x)*RotateY(_XYZ.y)*RotateZ(_XYZ.z);
	}

	mat4 Transpose()
	{
		return mat4( x.x, y.x, z.x, w.x, x.y, y.y, z.y, w.y, x.z, y.z, z.z, w.z, x.w, y.w, z.w, w.w );
	}

	static const mat4 Identity()
	{
		return mat4( 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
	}

	static mat4 Translate( float3 t )
	{
		return mat4( 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t.x, t.y, t.z, 1 );
	}

	void OrthoNormalize()
	{
		mat4 n;
		n.x.xyz = Normalize( Cross( y.xyz, z.xyz ) );
		n.y.xyz = Normalize( Cross( z.xyz, x.xyz ) );
		n.z.xyz = Normalize( Cross( x.xyz, y.xyz ) );
		n.w = w;
		*this = n;
	}

	static mat4 Scale( float3 s )
	{
		return mat4( s.x, 0, 0, 0, 0, s.y, 0, 0, 0, 0, s.z, 0, 0, 0, 0, 1 );
	}
	static mat4 Scale( float s )
	{
		return mat4( s, 0, 0, 0, 0, s, 0, 0, 0, 0, s, 0, 0, 0, 0, 1 );
	}

	mat4 Inverse()
	{
		float m00 = row[0][0], m01 = row[0][1], m02 = row[0][2], m03 = row[0][3];
		float m10 = row[1][0], m11 = row[1][1], m12 = row[1][2], m13 = row[1][3];
		float m20 = row[2][0], m21 = row[2][1], m22 = row[2][2], m23 = row[2][3];
		float m30 = row[3][0], m31 = row[3][1], m32 = row[3][2], m33 = row[3][3];

		float v0 = m20 * m31 - m21 * m30;
		float v1 = m20 * m32 - m22 * m30;
		float v2 = m20 * m33 - m23 * m30;
		float v3 = m21 * m32 - m22 * m31;
		float v4 = m21 * m33 - m23 * m31;
		float v5 = m22 * m33 - m23 * m32;

		float t00 = + (v5 * m11 - v4 * m12 + v3 * m13);
		float t10 = - (v5 * m10 - v2 * m12 + v1 * m13);
		float t20 = + (v4 * m10 - v2 * m11 + v0 * m13);
		float t30 = - (v3 * m10 - v1 * m11 + v0 * m12);

		float invDet = 1 / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

		float d00 = t00 * invDet;
		float d10 = t10 * invDet;
		float d20 = t20 * invDet;
		float d30 = t30 * invDet;

		float d01 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
		float d11 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
		float d21 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
		float d31 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

		v0 = m10 * m31 - m11 * m30;
		v1 = m10 * m32 - m12 * m30;
		v2 = m10 * m33 - m13 * m30;
		v3 = m11 * m32 - m12 * m31;
		v4 = m11 * m33 - m13 * m31;
		v5 = m12 * m33 - m13 * m32;

		float d02 = + (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
		float d12 = - (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
		float d22 = + (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
		float d32 = - (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

		v0 = m21 * m10 - m20 * m11;
		v1 = m22 * m10 - m20 * m12;
		v2 = m23 * m10 - m20 * m13;
		v3 = m22 * m11 - m21 * m12;
		v4 = m23 * m11 - m21 * m13;
		v5 = m23 * m12 - m22 * m13;

		float d03 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
		float d13 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
		float d23 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
		float d33 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

		return mat4(
			d00, d01, d02, d03,
			d10, d11, d12, d13,
			d20, d21, d22, d23,
			d30, d31, d32, d33);
	}
};

}; // namespace Tmpl8

#include "opencl.h"
#include "shader.h"
#include "gltools.h"
#include "texture.h"