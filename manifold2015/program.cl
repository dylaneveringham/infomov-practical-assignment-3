#define SCRWIDTH	1024
#define SCRHEIGHT	704

__kernel void RFFilterSplatsY(__global float4 *splat, __global float *dHdx, float a)
{
	for (int idx = get_global_id(0) * SCRWIDTH + 1, x = 1; x < SCRWIDTH; x++, idx++)
	{
		splat[idx] += pow(a, dHdx[idx]) * (splat[idx - 1] - splat[idx]);
	}
	for (int idx = get_global_id(0) * SCRWIDTH + SCRWIDTH - 2, x = SCRWIDTH - 2; x >= 0; x--, idx--)
	{
		splat[idx] += pow(a, splat[idx + 1]) * (splat[idx + 1] - splat[idx]);
	}
}

__kernel void RFFilterSplatsX(__global float4 *splat, __global float *dVdy, float a)
{
	for (int idx = get_global_id(0) + SCRWIDTH, y = 1; y < SCRHEIGHT; y++, idx += SCRWIDTH)
	{
		splat[idx] += pow(a, dVdy[idx]) * (splat[idx - SCRWIDTH] - splat[idx]);
	}
	for (int idx = get_global_id(0) + (SCRHEIGHT - 2) * SCRWIDTH, y = SCRHEIGHT - 2; y >= 0; y--, idx -= SCRWIDTH)
	{
		splat[idx] += pow(a, dVdy[idx + SCRWIDTH]) * (splat[idx + SCRWIDTH] - splat[idx]);
	}
}
