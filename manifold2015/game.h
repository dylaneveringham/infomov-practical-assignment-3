// Template, major revision 5
// IGAD/NHTV - Jacco Bikker - 2006-2014

#pragma once

#define SCRWIDTH	1024
#define SCRHEIGHT	704
#define SPP			2
#define PIXELCOUNT	(SCRWIDTH * SCRHEIGHT * SPP)
#define PIXELS		(SCRWIDTH * SCRHEIGHT)

namespace Tmpl8 {

class Surface;
class Game
{
public:
	void SetTarget( Surface* a_Surface ) { m_Screen = a_Surface; mdown = false; }
	void Init();
	void Tick( float a_DT );
	void HandleKeys();
	void KeyDown( unsigned int code ) {}
	void KeyUp( unsigned int code ) {}
	void MouseMove( unsigned int x, unsigned int y ) { mx = x, my = y; }
	void MouseUp( unsigned int button ) { mdown = false; }
	void MouseDown( unsigned int button ) { mdown = true; }
	// scenes
	void Scene1();
	void Scene2();
private:
	Surface* m_Screen;
	int mx, my;
	bool mdown;
};

}; // namespace Tmpl8